const express = require('express'),
    net = require('net'),
    basicAuth = require('express-basic-auth'),
    path = require('path'),
    router = express.Router(),
    WebSocket = require('ws');


const app = express();

app.get('/', basicAuth({
    challenge: true,
    users: {'root': '123'}
}));

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

app.use(router);


let clients = {};

const wss = new WebSocket.Server({
    port: 3002,
});

wss.on('connection', ws => {

    const id = Math.random();

    clients[id] = ws;

    ws.on('close', () => {
        console.log(`соединение закрыто ${id}`);
        delete clients[id];
    });

    ws.send(JSON.stringify({text: 'Вы успешно подлючились к сокет серверу'}));
});


const TCPServer = net.createServer(client => {

    client.setEncoding('utf8');

    client.on('data', data => {
        for (let key in clients) {
            clients[key].send(data);
        }

    });
});

TCPServer.maxConnections = 1;
TCPServer.listen(3001, 'localhost');

module.exports = app;