const https = require('https'),
    net = require('net'),
    client = new net.Socket();

const buildParams = params => {

    let result = [];

    Object.keys(params).forEach(key => {
        result.push(`${key}=${encodeURIComponent(params[key])}`);
    });

    return result.join('&')
};


const api_url = 'https://donatepay.ru/api/v1/transactions?',
    params = {
        access_token: '8Z2ATUiaAemOt5OUZD4mabV3RkWzq041dHwTs3MmsQoJst5mBqgowW8htrGd',
        limit: 1,
        before: '',
        after: '',
        skip: '',
        order: 'DESC',
        type: '',
        status: ''
    };

const url = api_url + buildParams(params),
    timerHandler = () => {

        https.get(url, response => { //Попытка скачать файл

            let body = '';

            response.on('data', chunk => {
                body += chunk;
            });

            response.on('end', () => {

                const jsonData = JSON.parse(body);

                if (jsonData && jsonData.status && jsonData.status === 'success') {

                    if (jsonData.data && jsonData.data.length > 0) {
                        const notice = jsonData.data[0];

                        if (notice.id !== last_notice_id) {
                            last_notice_id = notice.id;

                            if (client) {
                                client.write(JSON.stringify(notice));
                            }
                        }
                    }
                }

                setTimeout(timerHandler, 21000);
            });

        }).on('error', err => { //Ошибка получения response
            console.log(err);
        });


    };

let last_notice_id = false;

client.setEncoding('utf8');

client.connect({
    host: 'localhost',
    port: 3001
});

client.on('connect', () => {
    timerHandler();
});

client.on('error', data => {
    console.log(data.syscall === 'connect' ? 'Ошибка подключения.' : `Ошибка : ${data}`);
    process.exit();
});

client.on('data', data => {
    console.log(`Ответ сервера: ${data}`);
    prompt();
});
